
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

$.ajaxSetup({
  headers: {
      'Authorization': 'Basic ' + btoa('ois.seminar:ois4fri'),
  }
});

var pd = null;



function loadPatientDataById(id) {

	$.get(baseUrl + `/view/${id}/height`, height => {
		$.get(baseUrl + `/view/${id}/weight`, weight => {
			$.get(baseUrl + `/view/${id}/body_temperature`, bodyTemp => {
				$.get(baseUrl + `/view/${id}/blood_pressure`, bloodPressure => {
					// $.get(baseUrl + `/view/${id}/time`, time => {
						$.get(baseUrl + `/demographics/ehr/${id}/party`, r => {
							height.sort( (a, b) => new Date(a.time) - new Date(b.time) );
							weight.sort( (a, b) => new Date(a.time) - new Date(b.time) );
							bodyTemp.sort( (a, b) => new Date(a.time) - new Date(b.time) );
							bloodPressure.sort( (a, b) => new Date(a.time) - new Date(b.time) );
							pd = {height, weight, bodyTemp, bloodPressure, recordInfo: r.party};
							console.log(pd);
							updateView();
						});
					// });
				});
			});
		});
	});

}

function updateView() {
	$('#name').text(pd.recordInfo.firstNames + ' ' + pd.recordInfo.lastNames);
	$('#dateOfBirth').text(pd.recordInfo.dateOfBirth);
	$('#ehrId').text(pd.recordInfo.additionalInfo.ehrId);
	$('#weight').text(pd.weight.slice(-1)[0].weight + 'kg');
	$('#height').text(pd.height.slice(-1)[0].height + 'cm');
	$('#bodyTemp').text(pd.bodyTemp.slice(-1)[0].temperature + '°C');
	$('#lastCheckup').text('last checkup: ' + (''+new Date(pd.weight.slice(-1)[0].time)).substring(0, 16));
	displayBloodPressureChart(pd.bloodPressure);
	if (pd.bloodPressure.slice(-1)[0].systolic > 120 && pd.bloodPressure.slice(-1)[0].diastolic > 80) {
		$('#opozorilo').text('Imate povišan sistolični in diastolični krvni tlak! Če se stanje v naslednjih dneh ne izboljša, se naročite na pregled: 01 522 50 50!');
	}
	else if (pd.bloodPressure.slice(-1)[0].systolic > 120) {
		$('#opozorilo').text('Imate povišan sistolični krvni tlak! Če se stanje v naslednjih dneh ne izboljša, se naročite na pregled: 01 522 50 50!');
	}
	else if (pd.bloodPressure.slice(-1)[0].diastolic > 80) {
		$('#opozorilo').text('Imate povišan diastolični krvni tlak! Če se stanje v naslednjih dneh ne izboljša, se naročite na pregled: 01 522 50 50!');
	}
	else {
		$('#opozorilo').text('Vaš krvni tlak je v mejah normale.');
	}
	
}




var chart = null;

function displayBloodPressureChart(m) {
	if (chart) chart.destroy();

	var systolic = m.map( e => e.systolic );
	var diastolic = m.map( e => e.diastolic );
	var time = m.map( e =>  (new Date(e.time).getMonth()+1) + '.' + (new Date(e.time).getFullYear()) );

	var options = {
        chart: {
            height: 350,
            type: 'line',
            shadow: {
                enabled: true,
                color: '#000',
                top: 18,
                left: 7,
                blur: 10,
                opacity: 1
            },
            toolbar: {
                show: false
            }
        },
        colors: ['#77B6EA', '#545454'],
        dataLabels: {
            enabled: true,
        },
        stroke: {
            curve: 'smooth'
        },
        series: [
        	{
                name: "Systolic",
                data: systolic
            },
            {
                name: "Diastolic",
                data: diastolic
            },
        ],
        title: {
            text: 'Systolic and diastolic blood pressure',
            align: 'left'
        },
        grid: {
            borderColor: '#e7e7e7',
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        markers: {
            
            size: 6
        },
        xaxis: {
            categories: time,
            title: {
                text: 'Month.Year'
            }
        },
        yaxis: {
            title: {
                text: 'Pressure [mm(Hg)]'
            },
            min: 5,
            max: 200
        },
        legend: {
            position: 'top',
            horizontalAlign: 'right',
            floating: true,
            offsetY: -25,
            offsetX: -5
        }
    }

    chart = new ApexCharts(
        document.querySelector("#chart"),
        options
    );

    chart.render();
}

var mockPatients = [{},
	{
		partyData: {
		  firstName: "Marko",
		  lastName: "Pibernik",
		  dateOfBirth: "1982-07-18",
		},
		measurements: [
			{
				time: '2015-11-11',
				weight: 90,
				height: 190,
				temperature: 36.7,
				systolic: 120,
				diastolic: 80,
			}, {
				time: '2016-01-12',
				weight: 92,
				height: 190,
				temperature: 34.2,
				systolic: 118,
				diastolic: 85,
			}, {
				time: '2016-05-04',
				weight: 99,
				height: 190,
				temperature: 38.2,
				systolic: 135,
				diastolic: 91,
			}, {
				time: '2016-12-10',
				weight: 105,
				height: 190,
				temperature: 37.2,
				systolic: 118,
				diastolic: 75,
			}, {
				time: '2017-03-09',
				weight: 100,
				height: 190,
				temperature: 36.5,
				systolic: 150,
				diastolic: 90,
			},
		],
	}, {
		partyData: {
		  firstName: "Shaq",
		  lastName: "O'Neal",
		  dateOfBirth: "1972-03-06",
		},
		measurements: [
			{
				time: '2008-11-11',
				weight: 104,
				height: 216,
				temperature: 36.2,
				systolic: 110,
				diastolic: 67,
			}, {
				time: '2008-12-11',
				weight: 108,
				height: 216,
				temperature: 35.0,
				systolic: 118,
				diastolic: 85,
			}, {
				time: '2009-05-07',
				weight: 110,
				height: 216,
				temperature: 35.7,
				systolic: 135,
				diastolic: 75,
			}, {
				time: '2009-12-12',
				weight: 111,
				height: 216,
				temperature: 36.9,
				systolic: 120,
				diastolic: 80,
			}, {
				time: '2010-04-03',
				weight: 109,
				height: 216,
				temperature: 36.5,
				systolic: 118,
				diastolic: 86,
			},
		],
	}, {
		partyData: {
		  firstName: "Gloria",
		  lastName: "Borger",
		  dateOfBirth: "2010-09-22",
		},
		measurements: [
			{
				time: '2010-09-23',
				weight: 3,
				height: 45,
				temperature: 36.5,
				systolic: 110,
				diastolic: 59,
			}, {
				time: '2010-12-12',
				weight: 12,
				height: 67,
				temperature: 38.0,
				systolic: 115,
				diastolic: 60,
			}, {
				time: '2011-07-12',
				weight: 24,
				height: 89,
				temperature: 36.7,
				systolic: 120,
				diastolic: 80,
			}, {
				time: '2018-04-06',
				weight: 60,
				height: 169,
				temperature: 36.9,
				systolic: 122,
				diastolic: 86,
			}, {
				time: '2019-05-14',
				weight: 67,
				height: 180,
				temperature: 36.5,
				systolic: 120,
				diastolic: 80,
			},
		],
	}
];

function generirajPodatke(stPacienta, cb) {
	var d = mockPatients[stPacienta];
	var c = d.measurements.length;
	

	createParty(
		d.partyData,
		id => {
			for (var m of d.measurements) {
				commitMeasurements(id, m, () => {
					c--;
					if (!c) cb(id);
				});
			}
		},
	);
}

 

function createParty(data, cb) {
	$.post(
      baseUrl + "/ehr",
      r => {
          $.post({
              url: baseUrl + "/demographics/party",
              contentType: 'application/json',
              data: JSON.stringify({
				  firstNames: data.firstName,
				  lastNames: data.lastName,
				  dateOfBirth: data.dateOfBirth,
				  partyAdditionalInfo: [
				      {
				          key: "ehrId",
				          value: r.ehrId
				      }
				  ],
	  
			  }),
              success: party => {
                  if (party.action == 'CREATE') {
                  	cb(r.ehrId);
                  }
              }
          });
      }
  );
}


function commitMeasurements(ehrId, data, cb) {

	var composition = {
	    "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": data.time,
	    "vital_signs/body_weight/any_event/body_weight": data.weight,
	    "vital_signs/height_length/any_event/body_height_length": data.height,
	   	"vital_signs/body_temperature/any_event/temperature|magnitude": data.temperature,
	    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
	    "vital_signs/blood_pressure/any_event/systolic": data.systolic,
	    "vital_signs/blood_pressure/any_event/diastolic": data.diastolic,
	}

	$.post({
      url: baseUrl + `/composition?ehrId=${ehrId}&templateId=Vital+Signs`,
      contentType: 'application/json',
      data: JSON.stringify(composition),
      success: () => cb(ehrId),
	});
	
}


//console.log('!generiram podatke');

function asdf() {
	for (var n of [1,2,3]) {
		generirajPodatke(n, id => {
			$('#patientSelector').append(`<option value=${id}>${id}</option>`);
		});
	}
}
	


$('#patientSelector').on('change', e => loadPatientDataById(e.target.value));
$('#queryButton').click(() => loadPatientDataById($('#ehrIdInput').val()));
$('#patientSelector').click(e => loadPatientDataById($('#patientSelector').val()));
