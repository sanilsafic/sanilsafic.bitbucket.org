/* global L, distance */

var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;
var obmocje;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;

    function cmp(a, b) {
      return (distance(a._latlngs[0][0].lat, a._latlngs[0][0].lng, latlng.lat, latlng.lng) -
        distance(b._latlngs[0][0].lat, b._latlngs[0][0].lng, latlng.lat, latlng.lng))
    }

    polygons.sort(cmp);

    for (var p of polygons) {
      p.setStyle({color: 'blue'})
    }

    for (var i = 0; i < 3; i++) {
      polygons[i].setStyle({color: 'green'});
      // console.log(polygons[i])
    }

    // prikazPoti(latlng);
  }

  mapa.on('click', obKlikuNaMapo);
  

  pridobiPodatkeOBolnicah(function (jsonRezultat) {
    izrisRezultatov(jsonRezultat);
  });
});


function pridobiPodatkeOBolnicah(callback) {  
  var stevilo = 0;
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        callback(json);
    }
  };
  xobj.send(null);
}

var polygons = [];

/**
 * Na podlagi podanih interesnih točk v GeoJSON obliki izriši
 * posamezne točke na zemljevid
 * 
 * @param jsonRezultat interesne točke v GeoJSON obliki
 */
function izrisRezultatov(jsonRezultat) {
  for (var f of jsonRezultat.features) {

    if (f.geometry.type != "Polygon") continue;

    var latlngs = f.geometry.coordinates[0].map( a => [a[1], a[0]] );

    var polygon = L.polygon(latlngs, {color: 'blue'}).addTo(mapa);

    var naziv = f.properties.name ? f.properties.name : 'ni podatka';
    var naslov = (f.properties['addr:street'] && f.properties['addr:housenumber']) ? f.properties['addr:street'] + ' ' + f.properties['addr:housenumber'] : 'ni podatka';

    polygon.bindPopup(`<div>Naziv:&nbsp;&nbsp;&nbsp;${naziv}<br> Naslov: ${naslov}</div>`);

    polygons.push(polygon);

  }
}
